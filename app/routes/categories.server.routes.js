// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var users = require('../../app/controllers/users.server.controller'),
	categories = require('../../app/controllers/categories.server.controller');

// Define the routes module' method
module.exports = function(app) {
	// Set up the 'categories' base routes 
	app.route('/api/categories')
	   .get(categories.list)
	   .post(users.requiresLogin, categories.create);
	
	// Set up the 'categories' parameterized routes
	app.route('/api/categories/:categoryId')
	   .get(categories.read)
	   .put(users.requiresLogin, categories.hasAuthorization, categories.update)
	   .delete(users.requiresLogin, categories.hasAuthorization, categories.delete);

	// Set up the 'categoryId' parameter middleware   
	app.param('categoryId', categories.categoryByID);
};