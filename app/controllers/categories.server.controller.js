// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies
var mongoose = require('mongoose'),
	Category = mongoose.model('Category');

// Create a new error handling controller method
var getErrorMessage = function(err) {
	if (err.errors) {
		for (var errName in err.errors) {
			if (err.errors[errName].message) return err.errors[errName].message;
		}
	} else {
		return 'Unknown server error';
	}
};

// Create a new controller method that creates new categories
exports.create = function(req, res) {
	// Create a new category object
	var category = new Category(req.body);

	// Set the category's 'creator' property
	category.creator = req.user;

	// Try saving the category
	category.save(function(err) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the category 
			res.json(category);
		}
	});
};

// Create a new controller method that retrieves a list of categories
exports.list = function(req, res) {
	// Use the model 'find' method to get a list of categories
	Category.find().sort('-created').populate('creator', 'firstName lastName fullName').exec(function(err, categories) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the category 
			res.json(categories);
		}
	});
};

// Create a new controller method that returns an existing category
exports.read = function(req, res) {
	res.json(req.category);
};

// Create a new controller method that updates an existing category
exports.update = function(req, res) {
	// Get the category from the 'request' object
	var category = req.category;

	// Update the category fields
	category.title = req.body.title;
	category.content = req.body.content;

	// Try saving the updated category
	category.save(function(err) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the category 
			res.json(category);
		}
	});
};

// Create a new controller method that delete an existing category
exports.delete = function(req, res) {
	// Get the category from the 'request' object
	var category = req.category;

	// Use the model 'remove' method to delete the category
	category.remove(function(err) {
		if (err) {
			// If an error occurs send the error message
			return res.status(400).send({
				message: getErrorMessage(err)
			});
		} else {
			// Send a JSON representation of the category 
			res.json(category);
		}
	});
};

// Create a new controller middleware that retrieves a single existing category
exports.categoryByID = function(req, res, next, id) {
	// Use the model 'findById' method to find a single category 
	Category.findById(id).populate('creator', 'firstName lastName fullName').exec(function(err, category) {
		if (err) return next(err);
		if (!category) return next(new Error('Failed to load category ' + id));

		// If an category is found use the 'request' object to pass it to the next middleware
		req.category = category;

		// Call the next middleware
		next();
	});
};

// Create a new controller middleware that is used to authorize an category operation 
exports.hasAuthorization = function(req, res, next) {
	// If the current user is not the creator of the category send the appropriate error message
	if (req.category.creator.id !== req.user.id) {
		return res.status(403).send({
			message: 'User is not authorized'
		});
	}

	// Call the next middleware
	next();
};