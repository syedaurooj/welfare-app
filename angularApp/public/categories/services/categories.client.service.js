// Invoke 'strict' JavaScript mode
'use strict';

// Create the 'categories' service
angular.module('categories').factory('Categories', ['$resource', function($resource) {
	// Use the '$resource' service to return an category '$resource' object
    return $resource('api/categories/:categoryId', {
        categoryId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);